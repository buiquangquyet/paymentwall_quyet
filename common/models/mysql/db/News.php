<?php

namespace common\models\mysql\db;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $pubDate
 * @property string $link
 * @property int $guid
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['guid'], 'integer'],
            [['title', 'pubDate'], 'string', 'max' => 200],
            [['link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'pubDate' => 'Pub Date',
            'link' => 'Link',
            'guid' => 'Guid',
        ];
    }
}

<?php

namespace common\models\mysql\ModelSearch;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\mysql\db\News as NewsModel;

/**
 * News represents the model behind the search form of `common\models\mysql\db\News`.
 */
class News extends NewsModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'guid'], 'integer'],
            [['title', 'description', 'pubDate', 'link'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NewsModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'guid' => $this->guid,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'pubDate', $this->pubDate])
            ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }
}

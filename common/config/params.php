<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,

    'CACHE_USER_NAME_BY_ID_'        =>'CACHE_USER_NAME_BY_ID_',
    'CACHE_COURSE_ALL'              =>'CACHE_COURSE_ALL',
    'CACHE_COURSE_BY_ID_'           =>'CACHE_COURSE_BY_ID_',
    'CACHE_LESSION_ALL'             =>'CACHE_LESSION_ALL',
    'CACHE_LESSION_BY_ID_'          =>'CACHE_LESSION_BY_ID_',
    'CACHE_LESSION_BY_COURSE_ID_'   =>'CACHE_LESSION_BY_COURSE_ID_',

    'CACHE_NEWS_ALL' =>'CACHE_NEWS_ALL',
    'CACHE_NEWS_BY_ID_' =>'CACHE_NEWS_BY_ID_',
    'CACHE_SEARCH_DATA_PROVIDER_' =>'CACHE_SEARCH_DATA_PROVIDER_',
];

<?php
/**
 * Created by PhpStorm.
 * User: quangquyet
 * Date: 05/12/2018
 * Time: 08:44
 */

namespace common\components;


use yii\base\Model;
use yii\httpclient\Client;

class Vnexpress extends Model
{
    public $url;

    public function __construct()
    {

    }

    public function Getdata()
    {
        $url = 'https://vnexpress.net/rss/thoi-su.rss';
        $client = new Client();
        $response = $client->createRequest()
            ->setFormat(Client::FORMAT_JSON)
            ->setMethod('get')
            ->setUrl($this->url)
            ->send();
        $content = $response->getData();
        $data = !empty($content['channel']['item'])?$content['channel']['item']:[];
        return $data;
    }
}
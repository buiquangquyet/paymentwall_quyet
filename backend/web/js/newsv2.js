/**
 * Created by quangquyet on 05/12/2018.
 */
$(document).ready(function () {
    //$("tr.filters td input").click(function () {
    $(this).keyup(function (e) {
        if (e.keyCode == 13) {
            // var name = $(this).attr("name");
            // var value = ($(this).val());
            // console.log(name);
            // console.log(value);

            //var url = (URL_add_parameter(window.location.href, name, value));
            url = window.location.href;
            //redirectUrl(url);
            var count = $("tr.filters td input").length;

            $("tr.filters td input").each(function (i) {
                var input = $(this); // This is the jquery object of the input, do what you will
                var value = (input.val());
                var name = (input.attr("name"));

                url = (URL_add_parameter(url, name, value));

                if (i+1 === count) {
                    console.log(url);
                    redirectUrl(url);
                }
            });



        }
    });
    //});
    $(this).keyup(function (e) {

    });
});

function sortkey(key) {
    var data = getUrlParameter('sort');
    if(data == undefined){
        url = window.location.href;
        url = (URL_add_parameter(url, 'sort', key));
        url = (URL_add_parameter(url, 'value', 'asc'));
        //console.log(url);
    }else{
        if(data == key){
            var currentValue = getUrlParameter('value');
            if(currentValue == 'asc'){
                url = window.location.href;
                url = (URL_add_parameter(url, 'sort', key));
                url = (URL_add_parameter(url, 'value', 'desc'));
            }else{
                url = window.location.href;
                url = (URL_add_parameter(url, 'sort', key));
                url = (URL_add_parameter(url, 'value', 'asc'));
            }
            //console.log(url);
        }else{
            url = window.location.href;
            url = (URL_add_parameter(url, 'sort', key));
            url = (URL_add_parameter(url, 'value', 'asc'));
            //console.log(url);
        }
    }

    redirectUrl(url);
}

function getPage(page) {
    var data = getUrlParameter('page');
    url = window.location.href;
    url = (URL_add_parameter(url, 'page', page));

    redirectUrl(url);
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};


function URL_add_parameter(url, param, value) {
    var hash = {};
    var parser = document.createElement('a');

    parser.href = url;

    var parameters = parser.search.split(/\?|&/);

    for (var i = 0; i < parameters.length; i++) {
        if (!parameters[i])
            continue;

        var ary = parameters[i].split('=');
        hash[ary[0]] = ary[1];
    }

    hash[param] = value;

    var list = [];
    Object.keys(hash).forEach(function (key) {
        list.push(key + '=' + hash[key]);
    });

    parser.search = '?' + list.join('&');
    return parser.href;
}

function redirectUrl(url) {
    window.location.replace(url);
}
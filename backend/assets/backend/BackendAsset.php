<?php
/**
 * Created by PhpStorm.
 * User: quangquyet
 * Date: 05/12/2018
 * Time: 23:28
 */

namespace backend\assets\backend;


use backend\assets\AppAsset;
use yii\web\View;

class BackendAsset extends AppAsset
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $sourcePath = '@backend/assets/backend';

    public $css = [
        //'css/site.css',
    ];

    public $js = [
        //'js/app.js',
    ];

    public $jsOptions = [
        'position' => View::POS_END
    ];
    public $depends = [
        'backend\assets\PreAsset',
    ];

}
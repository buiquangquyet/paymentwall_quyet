<?php
/**
 * Created by PhpStorm.
 * User: quangquyet
 * Date: 05/12/2018
 * Time: 23:49
 */

namespace backend\assets;


use yii\web\AssetBundle;
use yii\web\View;

class PreAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets';

    public $css = [
        //'css/site.css',
    ];

    public $js = [
        'js/jquery.js',
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];
}
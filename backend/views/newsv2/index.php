<?php
/* @var $this yii\web\View */
use backend\assets\backend\BackendAsset;
use yii\web\View;

//$this->registerJsFile('/js/newsv2.js',['position' => \yii\web\View::POS_END]);
$this->registerJsFile('/js/newsv2.js', ['depends' => [BackendAsset::className()]]);
BackendAsset::register($this);
?>
<h1>newsv2/index</h1>
<p>
    <a class="btn btn-success" href="/newsv2/create">Create News</a></p>

<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th><a onclick="sortkey('id')" data-sort="id">ID</a></th>
        <th><a onclick="sortkey('title')" data-sort="title">Title</a></th>
        <th><a onclick="sortkey('pubDate')" data-sort="title">pubDate</a></th>
        <th><a onclick="sortkey('link')" data-sort="link">Link</a></th>
        <th class="action-column">&nbsp;</th>
    </tr>
    <tr id="w0-filters" class="filters">
        <td>&nbsp;</td>
        <?php
        ?>
        <td><input type="text" class="form-control" name="News[id]"
                   value="<?php echo !empty($params['News']['id']) ? $params['News']['id'] : '' ?>"></td>
        <td><input type="text" class="form-control" name="News[title]"
                   value="<?php echo !empty($params['News']['title']) ? $params['News']['title'] : '' ?>"></td>
        <td><input type="text" class="form-control" name="News[pubDate]"
                   value="<?php echo !empty($params['News']['pubDate']) ? $params['News']['pubDate'] : '' ?>"></td>
        <td><input type="text" class="form-control" name="News[link]"
                   value="<?php echo !empty($params['News']['link']) ? $params['News']['link'] : '' ?>"></td>
        <td>&nbsp;</td>
    </tr>
    </thead>
    <tbody>
    <?php if(!empty($data)) {
        foreach ($data as $key => $value) {
            ?>
            <tr data-key="<?= $value->id ?>">
                <td><?= $key ?></td>
                <td><?= $value->id ?></td>
                <td><?= $value->title ?></td>
                <td><?= $value->pubDate ?></td>
                <td><a href="<?= $value->link ?>"><?= $value->link ?></a></td>
                <td>
                    <a href="/newsv2/view?id=<?= $value->id ?>" title="View" aria-label="View" data-pjax="0"><span
                                class="glyphicon glyphicon-eye-open"></span></a>
                    <a href="/newsv2/update?id=<?= $value->id ?>" title="Update" aria-label="Update" data-pjax="0"><span
                                class="glyphicon glyphicon-pencil"></span></a>
                    <a href="/newsv2/delete?id=<?= $value->id ?>" title="Delete" aria-label="Delete" data-pjax="0"
                       data-confirm="Are you sure you want to delete this item?" data-method="post"><span
                                class="glyphicon glyphicon-trash"></span></a>
                </td>
            </tr>
        <?php }
    } ?>

    </tbody>

    <ul class="pagination">
        <li class="prev"><a href="/news/index?page=1&amp;sort=id" data-page="0">«</a></li>
        <?php for ($i = 1; $i<=$totalPage; $i++){ ?>
           <li class="<?php echo ($page == $i)?'active':''?>"><a onclick="getPage(<?=$i?>)" data-page="<?=$i?>"><?=$i?></a></li>
        <?php }?>
        <li class="next disabled"><span>»</span></li>
    </ul>
</table>
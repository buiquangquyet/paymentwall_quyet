<?php

namespace backend\controllers;

use common\models\LoginForm;
use Yii;

class AuthController extends \yii\web\Controller
{
    /**
     * AuthController constructor.
     * @param string           $id
     * @param \yii\base\Module $module
     * @param array            $config
     */
    public function __construct($id, \yii\base\Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);

        if(empty(Yii::$app->user->getId())) {
            return $this->goBack('/site/login');
        }
    }
}

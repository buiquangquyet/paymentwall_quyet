<?php

namespace backend\controllers;

use common\models\mysql\modeldb\News;
use Yii;

class Newsv2Controller extends AuthController
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $per_page = News::PERPAGE;
        $searchModel = new News();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $data = $dataProvider->query->all();
        $countitem = $dataProvider->query->count();
        $totalPage = ceil($countitem/$per_page);
        $page = !empty(Yii::$app->request->queryParams['page'])?Yii::$app->request->queryParams['page']:1;
        //$searchModel->getData($dataProvider,Yii::$app->request->queryParams,false);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => $data,
            'params' => Yii::$app->request->queryParams,
            'totalPage'=>$totalPage,
            'page'=>$page
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {

        $model = new News();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
//        $model = News::find($id);
//        $model->delete();
//        $model = News::find()->where(['id'=>$id])->one();
//        $rs = $model->delete();

        $model = $this->findModel($id);

        if (!($model->delete()))
        {
            Yii::$app->session->setFlash('error', 'Unable to delete model');
            print_r(Yii::$app->session->getFlash('error'));
            var_dump($model->delete());
            die();
        }
        return $this->redirect(['/newsv2/index']);
    }


    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $model = News::getByPk($id);
        return $this->render('view',[
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return null|static
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}

/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : paymentwall_quyetbq

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2018-12-06 04:38:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', '1520216754');
INSERT INTO `migration` VALUES ('m130524_201442_init', '1520216759');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `pubDate` varchar(200) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `guid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('4', 'Bí thư Sóc Sơn: Nói phá Việt phủ Thành Chương là vô cảm', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/bi-thu-soc-son-noi-pha-viet-phu-thanh-chuong-la-vo-cam-3849147.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/PhamXuanPhuong-1543919819-5106-1543920314_180x108.jpg\" ></a></br>Ông Phạm Xuân Phương cho rằng, đây là công trình hiếm về văn hoá Việt cổ, cần có cơ chế để thành điểm du lịch tâm linh hợp pháp.', 'Tue, 04 Dec 2018 20:55:21 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/bi-thu-soc-son-noi-pha-viet-phu-thanh-chuong-la-vo-cam-3849147.html', '0');
INSERT INTO `news` VALUES ('5', 'Xe container tông xe tải, hầm Hải Vân tê liệt bốn tiếng', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/xe-container-tong-xe-tai-ham-hai-van-te-liet-bon-tieng-3849177.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/tai-nan-3701-1543925362_180x108.jpg\" ></a></br>Tai nạn giữa hai ôtô trong hầm Hải Vân đã khiến các phương tiện xếp hàng dài 3-4 km trong nhiều tiếng.', 'Tue, 04 Dec 2018 19:58:53 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/xe-container-tong-xe-tai-ham-hai-van-te-liet-bon-tieng-3849177.html', '0');
INSERT INTO `news` VALUES ('6', 'Nhân viên an ninh sân bay Thọ Xuân bị phạt vì \'phản ứng chậm\' ', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/nhan-vien-an-ninh-san-bay-tho-xuan-bi-phat-vi-phan-ung-cham-3848561.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/1-1543916198-2312-1543916557_180x108.jpg\" ></a></br>Khi diễn ra vụ gây rối ở sân bay Thọ Xuân, bốn nhân viên an ninh chưa thực hiện đúng nhiệm vụ được giao.', 'Tue, 04 Dec 2018 16:44:18 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/nhan-vien-an-ninh-san-bay-tho-xuan-bi-phat-vi-phan-ung-cham-3848561.html', '0');
INSERT INTO `news` VALUES ('7', 'TP HCM lấy phiếu tín nhiệm 30 cán bộ chủ chốt', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/tp-hcm-lay-phieu-tin-nhiem-30-can-bo-chu-chot-3849077.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/HDND1373591672500x061541418955-4959-9322-1543915769_180x108.jpg\" ></a></br>HĐND TP HCM chiều mai (5/12) sẽ bỏ phiếu tín nhiệm Chủ tịch và 4 Phó chủ tịch UBND TP HCM và hàng loạt lãnh đạo các sở ngành.', 'Tue, 04 Dec 2018 16:29:29 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/tp-hcm-lay-phieu-tin-nhiem-30-can-bo-chu-chot-3849077.html', '0');
INSERT INTO `news` VALUES ('8', 'Ba du khách tử vong ở Đà Nẵng trúng chất độc ảnh hưởng tim mạch', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/ba-du-khach-tu-vong-o-da-nang-trung-chat-doc-anh-huong-tim-mach-3849019.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/van67871537845701-1543912296-4181-1543912302_180x108.jpg\" ></a></br>Hơn 2 tháng sau vụ ba du khách tử vong ở Đà Nẵng, cơ quan Pháp y mới đưa ra kết luận ban đầu do \"rất khó tìm nguyên nhân\".', 'Tue, 04 Dec 2018 15:33:44 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/ba-du-khach-tu-vong-o-da-nang-trung-chat-doc-anh-huong-tim-mach-3849019.html', '0');
INSERT INTO `news` VALUES ('9', 'Ông Lê Mạnh Hà bị khiển trách', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/ong-le-manh-ha-bi-khien-trach-3848975.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/LeManhHa087291461298006-154390-4834-1840-1543907117_180x108.jpg\" ></a></br>Nguyên Phó chủ nhiệm Văn phòng Chính phủ có trách nhiệm liên quan trong dự án MobiFone mua 95% cổ phần AVG.', 'Tue, 04 Dec 2018 14:06:49 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/ong-le-manh-ha-bi-khien-trach-3848975.html', '0');
INSERT INTO `news` VALUES ('10', 'Quảng Ninh sáp nhập bốn cơ quan báo chí', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/quang-ninh-sap-nhap-bon-co-quan-bao-chi-3848924.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/tru-so-3452-1543906421_180x108.jpg\" ></a></br>Từ năm 2019, báo Quảng Ninh, Đài Phát thanh Truyền hình tỉnh, báo Hạ Long và Cổng Thông tin điện tử sẽ sáp nhập làm một.', 'Tue, 04 Dec 2018 13:53:52 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/quang-ninh-sap-nhap-bon-co-quan-bao-chi-3848924.html', '0');
INSERT INTO `news` VALUES ('11', 'Ông Nguyễn Thiện Nhân trăn trở vì \'bà con Thủ Thiêm về nơi cũ dựng lều\'', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/ong-nguyen-thien-nhan-tran-tro-vi-ba-con-thu-thiem-ve-noi-cu-dung-leu-3848852.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/nguyenthiennhan1-1543900603-9969-1543902568_180x108.jpg\" ></a></br>Trước việc một số hộ dân quay về phần đất cũ của mình dựng lều ở, Bí thư Thành ủy TP HCM cho rằng cách này không giải quyết được căn cơ.', 'Tue, 04 Dec 2018 12:49:27 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/ong-nguyen-thien-nhan-tran-tro-vi-ba-con-thu-thiem-ve-noi-cu-dung-leu-3848852.html', '0');
INSERT INTO `news` VALUES ('12', 'Cháy căn hộ tầng ba khu tập thể cũ ở Hà Nội', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/chay-can-ho-tang-ba-khu-tap-the-cu-o-ha-noi-3848902.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/chaykhutt-1543899315-4975-1543899389_180x108.jpg\" ></a></br>Sau khoảng 30 phút, lửa đã thiêu rụi hai căn hộ liền kề của khu tập thể A12 Khương Thượng. ', 'Tue, 04 Dec 2018 12:00:36 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/chay-can-ho-tang-ba-khu-tap-the-cu-o-ha-noi-3848902.html', '0');
INSERT INTO `news` VALUES ('13', 'Xe tải đâm vào dải phân cách, đường vành đai ba ở Hà Nội ùn tắc', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/giao-thong/xe-tai-dam-vao-dai-phan-cach-duong-vanh-dai-ba-o-ha-noi-un-tac-3848806.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/473228001984982578255465901622-7274-8488-1543893758_180x108.jpg\" ></a></br>Xe tải chở xi măng lật nghiêng, chắn ngang đường vành đai ba từ 1h đến 7h khiến giao thông trên tuyến bị tê liệt. ', 'Tue, 04 Dec 2018 11:07:41 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/giao-thong/xe-tai-dam-vao-dai-phan-cach-duong-vanh-dai-ba-o-ha-noi-un-tac-3848806.html', '0');
INSERT INTO `news` VALUES ('14', 'Người phụ nữ trả lại hơn 80 triệu đồng nhặt được', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/nguoi-phu-nu-tra-lai-hon-80-trieu-dong-nhat-duoc-3848741.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/CHINHO4-1543889735-8701-1543889958_180x108.jpg\" ></a></br>Trên đường đi nhặt phế liệu, bà Nhỡ (Đồng Tháp) phát hiện chiếc túi có nhiều tiền, liền mang đến công an nhờ tìm người đánh rơi trả lại.', 'Tue, 04 Dec 2018 10:40:03 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/nguoi-phu-nu-tra-lai-hon-80-trieu-dong-nhat-duoc-3848741.html', '0');
INSERT INTO `news` VALUES ('15', 'Lãnh đạo TP HCM sẽ bị chất vấn về \'lời hứa\' trước cử tri', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/lanh-dao-tp-hcm-se-bi-chat-van-ve-loi-hua-truoc-cu-tri-3848773.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/quyettam-1543895165-3686-1543895195_180x108.jpg\" ></a></br>Theo Chủ tịch HĐND TP HCM, kinh tế thành phố tăng trưởng nhưng còn nhiều vấn đề chưa làm được nên các đại biểu sẽ chất vấn để làm rõ.', 'Tue, 04 Dec 2018 10:37:59 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/lanh-dao-tp-hcm-se-bi-chat-van-ve-loi-hua-truoc-cu-tri-3848773.html', '0');
INSERT INTO `news` VALUES ('16', 'Trại nuôi ruồi lấy trứng thu hơn trăm triệu đồng mỗi tháng', '<a href=\"https://vnexpress.net/photo/thoi-su/trai-nuoi-ruoi-lay-trung-thu-hon-tram-trieu-dong-moi-thang-3848493.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/03/ruoilinhden-7-1543826491_180x108.jpg\" ></a></br>Trứng của ruồi lính đen nở thành nhộng là thức ăn giàu dinh dưỡng trong chăn nuôi, được ông Bé (Long An) bán với giá 20 triệu đồng một cân.', 'Tue, 04 Dec 2018 00:00:00 +0700', 'https://vnexpress.net/photo/thoi-su/trai-nuoi-ruoi-lay-trung-thu-hon-tram-trieu-dong-moi-thang-3848493.html', '0');
INSERT INTO `news` VALUES ('17', 'Cấp đất sai cho vợ lãnh đạo, bí thư huyện bị kỷ luật', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/cap-dat-sai-cho-vo-lanh-dao-bi-thu-huyen-bi-ky-luat-3849022.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/toptop-1543910574-4987-1543910634_180x108.jpg\" ></a></br>Khi làm Phó chủ tịch huyện Kiên Lương (Kiên Giang), ông Dũng ký quyết định cấp đất cho vợ của bí thư huyện ủy đương nhiệm sai quy định.', 'Tue, 04 Dec 2018 20:26:13 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/cap-dat-sai-cho-vo-lanh-dao-bi-thu-huyen-bi-ky-luat-3849022.html', '0');
INSERT INTO `news` VALUES ('18', 'Phó chủ tịch Quốc hội đề nghị Hà Nội quan tâm \'chọn nguồn cán bộ\'', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/pho-chu-tich-quoc-hoi-de-nghi-ha-noi-quan-tam-chon-nguon-can-bo-3848933.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/201712181556345984NDN0750-1543-4141-3808-1543908904_180x108.jpg\" ></a></br>Bà Tòng Thị Phóng nói HĐND TP Hà Nội có trách nhiệm tạo nguồn cán bộ cho thành phố, Quốc hội và các cơ quan Trung ương.', 'Tue, 04 Dec 2018 14:35:42 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/pho-chu-tich-quoc-hoi-de-nghi-ha-noi-quan-tam-chon-nguon-can-bo-3848933.html', '0');
INSERT INTO `news` VALUES ('19', 'Trình quy hoạch Ban chấp hành Trung ương trong tháng 12', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/trinh-quy-hoach-ban-chap-hanh-trung-uong-trong-thang-12-3848555.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/04/PhamMinhChinh1JPG6119154296252-8564-7463-1543856740_180x108.jpg\" ></a></br>Lãnh đạo Ban Tổ chức Trung ương nhấn mạnh yêu cầu chống chạy chức, chạy quyền trong công tác cán bộ.', 'Tue, 04 Dec 2018 00:06:23 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/trinh-quy-hoach-ban-chap-hanh-trung-uong-trong-thang-12-3848555.html', '0');
INSERT INTO `news` VALUES ('20', 'Hộ dân ở Hải Dương kiếm tiền triệu mỗi ngày nhờ vườn cúc hoạ mi', '<a href=\"https://vnexpress.net/photo/thoi-su/ho-dan-o-hai-duong-kiem-tien-trieu-moi-ngay-nho-vuon-cuc-hoa-mi-3848624.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/03/68-1543842521_180x108.jpg\" ></a></br>Mở cửa trong 9 ngày, vườn cúc hoạ mi rộng hơn 3.000 m2 ở Hải Dương đón tổng cộng cả nghìn lượt khách tham quan.', 'Tue, 04 Dec 2018 00:00:00 +0700', 'https://vnexpress.net/photo/thoi-su/ho-dan-o-hai-duong-kiem-tien-trieu-moi-ngay-nho-vuon-cuc-hoa-mi-3848624.html', '0');
INSERT INTO `news` VALUES ('21', 'Cao tốc Lạng Sơn - Cao Bằng dài 115 km được trình Chính phủ', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/giao-thong/cao-toc-lang-son-cao-bang-dai-115-km-duoc-trinh-chinh-phu-3848577.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/03/caotoc1-1543848687-9971-1543849398_180x108.jpg\" ></a></br>Tổng mức đầu tư dự án khoảng hơn 20.000 tỷ đồng, giảm hơn 26.000 tỷ đồng so với nghiên cứu của các nhà đầu tư trước đó.', 'Tue, 04 Dec 2018 00:00:00 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/giao-thong/cao-toc-lang-son-cao-bang-dai-115-km-duoc-trinh-chinh-phu-3848577.html', '0');
INSERT INTO `news` VALUES ('22', 'BOT ở TP HCM xả trạm vì tài xế phản đối thu phí quá hạn', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/bot-o-tp-hcm-xa-tram-vi-tai-xe-phan-doi-thu-phi-qua-han-3848618.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/03/xa-tram-2-1543846526-9702-1543846909_180x108.jpg\" ></a></br>Cho rằng trạm thu phí An Sương - An Lạc (quận Bình Tân) hoạt động quá hạn 31 tháng, nhóm tài xế đậu xe phản đối.', 'Mon, 03 Dec 2018 21:41:23 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/bot-o-tp-hcm-xa-tram-vi-tai-xe-phan-doi-thu-phi-qua-han-3848618.html', '0');
INSERT INTO `news` VALUES ('23', 'Sinh viên Sài Gòn ùn ùn đăng ký thi TOEIC từ rạng sáng', '<a href=\"https://vnexpress.net/tin-tuc/giao-duc/sinh-vien-sai-gon-un-un-dang-ky-thi-toeic-tu-rang-sang-3848412.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/03/xephangthi-1543824777-8236-1543828136_180x108.jpg\" ></a></br>Lo đề thi TOEIC sắp thay đổi cấu trúc sẽ khó vượt qua, hàng trăm sinh viên rồng rắn xếp hàng từ 4h để đăng ký thi trước giờ G.', 'Mon, 03 Dec 2018 19:37:52 +0700', 'https://vnexpress.net/tin-tuc/giao-duc/sinh-vien-sai-gon-un-un-dang-ky-thi-toeic-tu-rang-sang-3848412.html', '0');
INSERT INTO `news` VALUES ('24', 'Tôm hùm nuôi trên vịnh Cam Ranh chết hàng loạt', '<a href=\"https://vnexpress.net/tin-tuc/thoi-su/tom-hum-nuoi-tren-vinh-cam-ranh-chet-hang-loat-3848575.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/03/1vnetomchet1-1543834675-2804-1543836383_180x108.jpg\" ></a></br>Khoảng 100 hộ dân ở Khánh Hòa đang điêu đứng khi tôm hùm nuôi bán trong dịp Tết bất ngờ chết chưa rõ nguyên nhân.', 'Mon, 03 Dec 2018 19:34:07 +0700', 'https://vnexpress.net/tin-tuc/thoi-su/tom-hum-nuoi-tren-vinh-cam-ranh-chet-hang-loat-3848575.html', '0');
INSERT INTO `news` VALUES ('25', 'Ông Mai Tiến Dũng: ‘Số nợ nhà thầu ở dự án Metro TP HCM không quá nhiều’', '<a href=\"https://vnexpress.net/tong-thuat/thoi-su/ong-mai-tien-dung-so-no-nha-thau-o-du-an-metro-tp-hcm-khong-qua-nhieu-3848570.html\"><img width=130 height=100 src=\"https://i-vnexpress.vnecdn.net/2018/12/03/MaiTienDung4-1543837922-1721-1543837927_180x108.jpg\" ></a></br>Tổng mức đầu tư ban đầu của dự án Metro số 1 TP HCM  là 17.388 tỷ đồng và đến nay đã giải ngân đạt 52%.', 'Mon, 03 Dec 2018 17:29:22 +0700', 'https://vnexpress.net/tong-thuat/thoi-su/ong-mai-tien-dung-so-no-nha-thau-o-du-an-metro-tp-hcm-khong-qua-nhieu-3848570.html', '0');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE,
  UNIQUE KEY `email` (`email`) USING BTREE,
  UNIQUE KEY `password_reset_token` (`password_reset_token`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'buiquangquyet@gmail.com', 'QuxIrvkWyhUbGVopvD30xib99brLfkaC', '$2y$13$hHPsArq5aPRgj9Qypp5oyuSBLi3v8K108iFG2c.NCy9XiSqtWMTU6', null, 'buiquangquyet@gmail.com', '10', '1539165860', '1539165860');
INSERT INTO `user` VALUES ('2', 'hanhhtm', 'gim-B8aDpnkfataZjwP-rRYLLnidv2Mn', '$2y$13$8gA2loVkBgObFz7eTt/Mk.EY8wXQ2BJLn8fQopbhFb2GOFWdBbTVS', null, 'hanhhtm@peacesoft.net', '10', '1539166341', '1539166341');
INSERT INTO `user` VALUES ('3', 'thuykan101296@gmail.com', 'DTiTLcZxWsBg244O9vGMlaW-lvLkMbfz', '$2y$13$NyWz9ZkRV1W4GtSyZVY9U.tmzBFkQsqXc7/UDTHd6f7h87SiHDE5i', null, 'thuykan101296@gmail.com', '10', '1540283187', '1540283187');
SET FOREIGN_KEY_CHECKS=1;

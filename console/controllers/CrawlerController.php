<?php
/**
 * Created by PhpStorm.
 * User: quangquyet
 * Date: 04/12/2018
 * Time: 21:11
 */

namespace console\controllers;


use common\components\Vnexpress;
use common\models\mysql\modeldb\News;
use yii\console\Controller;
use yii\console\widgets\Table;
use yii\httpclient\Client;

class CrawlerController extends Controller
{

    public function actionGetdata(){
        $data = new Vnexpress();
        $data->url = 'https://vnexpress.net/rss/thoi-su.rss';
        $data = $data->Getdata();
        if(!empty($data)){
            foreach ($data as $key=>$val){
                $Modelnews= new News();
                $Modelnews->setAttributes($val, false);
                if ($Modelnews->validate()) {
                    $Modelnews->save();
                    echo 'Created new news id : '. $Modelnews->id;
                } else {
                    // validation failed: $errors is an array containing error messages
                    $errors = $Modelnews->errors;
                    print_r($errors);
                }
            }
        }
    }

    public function actionShowdata(){
        $Modelnews = News::getAll(false);
        if(!empty($Modelnews)){
            foreach ($Modelnews as $key=>$val){
                $rows[$key][]= $val->id;
                $rows[$key][]= $val->title;
                $rows[$key][]= $val->pubDate;
            }
        }

        echo Table::widget([
            'headers' => ['id', 'title','pubDate'],
            'rows' => $rows
        ]);
    }

}